# LAN Dinner Party
This is getting complicated, so here's a rough schedule of how I'm imagining things will go down:

### Tuesday-Thurs "Plan" (Never Implemented)
* Pick a processor & motherboard
    * I want type C.
    * At least 4 SATA ports.
    * 2 M.2 Slots.
    * Needs to be compatible with DDR4.
* Buy 25' ethernet cable, switch, smaller cables.
* Ask roommates if anyone can do a Microcenter pickup from Cinci.


### Thursday Night
* Went to Microcenter.
* Got i5, motherboard, all parts.
* N's car broke on way back to assemle. Rescheduled.

### Friday Night
* Cooler doesn't fit 12th-gen processor bracket, so that's an issue.
* Need screws for motherboard => case. Went to home depot, but nothing similar enough.
* Ended up eating pizza & having a good time playing borderlands.
